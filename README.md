# Supply API Gateway for Proca

[![pipeline status](https://gitlab.com/szakdolgozat-rjlpoo/proca-supply-gateway/badges/master/pipeline.svg)](https://gitlab.com/szakdolgozat-rjlpoo/proca-supply-gateway/commits/master)
[![coverage report](https://gitlab.com/szakdolgozat-rjlpoo/proca-supply-gateway/badges/master/coverage.svg)](https://gitlab.com/szakdolgozat-rjlpoo/proca-supply-gateway/commits/master)

## Docker

### Environment variables

#### `PROCA_SUPPLY_GATEWAY_SOURCE`

Source to the users CSV.

Default: `/proca/users.csv`

#### `PROCA_SUPPLY_GATEWAY_TARGET`

Target of the reverse proxy.

Default: `http://localhost:8120`

#### `PROCA_SUPPLY_GATEWAY_CLIENT_ID`

Auth0 client ID.

Required

#### `PROCA_SUPPLY_GATEWAY_CLIENT_SECRET`

Auth0 client secret.

Required

#### `PROCA_SUPPLY_GATEWAY_AUIDENCE`

Auth0 audience.

Required

#### `PROCA_SUPPLY_GATEWAY_GRANT_TYPE`

Auth0 grant type.

Required

## Endpoints

### Read all products

```
GET /products
Protected with basic auth

Proxy for:
/products 
```

### Read product

```
GET /products/{product-id}
Protected with basic auth

Proxy for:
/products/{product-id}
```

### Read all prices

```
GET /products/{product-id}/prices
Protected with basic auth

Proxy for:
/products/{product-id}/suppliers/{supplier-id}/prices 
```

### Read price

```
GET /products/{product-id}/prices/{price-id}
Protected with basic auth

Proxy for:
/products/{product-id}/suppliers/{supplier-id}/prices
```

### Create price

```
POST /products/{product-id}/prices
Protected with basic auth

Proxy for:
/products/{product-id}/suppliers/{supplier-id}/prices
```

### Update price

```
PUT /products/{product-id}/prices/{price-id}
Protected with basic auth

Proxy for:
/products/{product-id}/suppliers/{supplier-id}/prices
```
