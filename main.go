package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"net/http/httputil"
	"net/url"
	"os"
)

var (
	port   = flag.Int("port", 8210, "port for the web application")
	source = flag.String("source", "users.csv", "CSV source of users")

	target = flag.String("target", "http://localhost:8120", "host, and base path for the proxy")

	tokenEndpoint = flag.String("token-endpoint", "https://dev-4vn5kbvn.eu.auth0.com/oauth/token", "endpoint for JWT token")
	clientId      = flag.String("client-id", "", "client ID for requesting access token")
	clientSecret  = flag.String("client-secret", "", "client secret for requesting access token")
	audience      = flag.String("audience", "", "audience for requesting access token")
	grantType     = flag.String("grant-type", "", "grant type for requesting access token")
)

func main() {
	flag.Parse()

	fmt.Println(fmt.Sprintf("Token endpoint: %s", *tokenEndpoint))
	fmt.Println(fmt.Sprintf("User source: %s", *source))
	fmt.Println(fmt.Sprintf("Target URL: %s", *target))

	var err error
	var s Supply

	s.client = &Auth0Client{
		TokenEndpoint: *tokenEndpoint,
		ClientId:      *clientId,
		ClientSecret:  *clientSecret,
		Audience:      *audience,
		GrantType:     *grantType,
	}

	file, err := os.Open(*source)
	if err != nil {
		panic(err)
	}

	s.repository, err = InMemoryRepositoryFromCsv(file)
	if err != nil {
		panic(err)
	}

	targetUrl, err := url.Parse(*target)
	if err != nil {
		panic("Target flag must contain a valid url")
	}

	s.proxy = httputil.NewSingleHostReverseProxy(targetUrl)

	http.HandleFunc("/", s.Handler)

	fmt.Println(fmt.Sprintf("Application listening at: %d", *port))
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", *port), nil))
}
