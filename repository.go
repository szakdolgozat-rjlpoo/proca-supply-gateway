package main

import (
	"encoding/csv"
	"errors"
	"fmt"
	"io"
)

var NotFound = errors.New("resource not found")

type User struct {
	Id       string
	Username string
	Password string
}

type Repository interface {
	findUserByName(username string) (s User, err error)
}

type InMemoryRepository struct {
	users []User
}

func (i *InMemoryRepository) findUserByName(username string) (s User, err error) {
	for _, v := range i.users {
		if v.Username == username {
			return v, nil
		}
	}
	return s, NotFound
}

func InMemoryRepositoryFromCsv(r io.Reader) (*InMemoryRepository, error) {
	if r == nil {
		return nil, fmt.Errorf("reader must not be nil")
	}

	reader := csv.NewReader(r)

	var users []User
	for i := 0; ; i++ {
		record, err := reader.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			return nil, fmt.Errorf("error on row %d: %w", i, err)
		}

		expectedColumns := 3
		if len(record) != expectedColumns {
			return nil, fmt.Errorf("error on row %d: row should contain %d columns", i, expectedColumns)
		}

		users = append(users, User{
			Id:       record[0],
			Username: record[1],
			Password: record[2],
		})
	}

	return &InMemoryRepository{users: users}, nil
}
