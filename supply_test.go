package main

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"net/http/httputil"
	"net/url"
	"testing"
)

type AccessTokenClientMock struct {
	getAccessTokenMethod func() string
}

func (a AccessTokenClientMock) GetAccessToken() string {
	return a.getAccessTokenMethod()
}

func TestSupply_Handler(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, _ = w.Write([]byte("{}\n"))
	}))
	target, _ := url.Parse(ts.URL)
	invalidUrl, _ := url.Parse("")
	validUrl, _ := url.Parse("/products/1/prices")

	type fields struct {
		client     AccessTokenClient
		repository Repository
		proxy      http.Handler
	}
	tests := []struct {
		name        string
		fields      fields
		request     *http.Request
		wantStatus  int
		wantBody    string
		wantURLPath string
	}{
		{
			name: "Without auth",
			fields: fields{
				client: AccessTokenClientMock{func() string {
					return "access-token"
				}},
				repository: &InMemoryRepository{users: []User{}},
				proxy:      httputil.NewSingleHostReverseProxy(target),
			},
			request:    &http.Request{URL: invalidUrl},
			wantStatus: 403,
			wantBody:   "403 Forbidden\n",
		},
		{
			name: "With invalid username",
			fields: fields{
				client: AccessTokenClientMock{func() string {
					return "access-token"
				}},
				repository: &InMemoryRepository{users: []User{}},
				proxy:      httputil.NewSingleHostReverseProxy(target),
			},
			request: &http.Request{
				URL:    invalidUrl,
				Header: map[string][]string{"Authorization": {"Basic dGVzdDp0ZXN0"}},
			},
			wantStatus: 403,
			wantBody:   "403 Forbidden\n",
		},
		{
			name: "With invalid password",
			fields: fields{
				client: AccessTokenClientMock{func() string {
					return "access-token"
				}},
				repository: &InMemoryRepository{users: []User{{Username: "test"}}},
				proxy:      httputil.NewSingleHostReverseProxy(target),
			},
			request: &http.Request{
				URL:    invalidUrl,
				Header: map[string][]string{"Authorization": {"Basic dGVzdDp0ZXN0"}},
			},
			wantStatus: 403,
			wantBody:   "403 Forbidden\n",
		},
		{
			name: "With invalid url",
			fields: fields{
				client: AccessTokenClientMock{func() string {
					return "access-token"
				}},
				repository: &InMemoryRepository{users: []User{
					{Id: "id", Username: "test", Password: "$2a$10$UBl8pqWu8VOfbgFtueDYdeDgU3qpGRbj0eEn.Ahe92L0mysoaaorK"}},
				},
				proxy: httputil.NewSingleHostReverseProxy(target),
			},
			request: &http.Request{
				URL:    invalidUrl,
				Header: map[string][]string{"Authorization": {"Basic dGVzdDp0ZXN0"}},
			},
			wantStatus: 404,
			wantBody:   "404 page not found\n",
		},
		{
			name: "With valid url",
			fields: fields{
				client: AccessTokenClientMock{func() string {
					return "access-token"
				}},
				repository: &InMemoryRepository{users: []User{
					{Id: "test-id", Username: "test", Password: "$2a$10$UBl8pqWu8VOfbgFtueDYdeDgU3qpGRbj0eEn.Ahe92L0mysoaaorK"}},
				},
				proxy: httputil.NewSingleHostReverseProxy(target),
			},
			request: &http.Request{
				URL:    validUrl,
				Header: map[string][]string{"Authorization": {"Basic dGVzdDp0ZXN0"}},
			},
			wantStatus:  200,
			wantBody:    "{}\n",
			wantURLPath: "/products/1/suppliers/test-id/prices",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &Supply{
				client:     tt.fields.client,
				repository: tt.fields.repository,
				proxy:      tt.fields.proxy,
			}
			recorder := httptest.NewRecorder()
			s.Handler(recorder, tt.request)
			if got := recorder.Code; got != tt.wantStatus {
				t.Errorf("rewritePath() got = %v, want %v", got, tt.wantStatus)
				return
			}
			bytes, _ := ioutil.ReadAll(recorder.Body)
			if got := string(bytes); got != tt.wantBody {
				t.Errorf("rewritePath() got = %v, want %v", got, tt.wantBody)
				return
			}
			if got := tt.request.URL.Path; got != tt.wantURLPath {
				t.Errorf("rewritePath() got = %v, want %v", got, tt.wantURLPath)
				return
			}
		})
	}
}

func Test_rewritePath(t *testing.T) {
	type args struct {
		path []byte
		id   string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{
			name: "modifies path for products",
			args: args{path: []byte("/products"), id: "object-id"},
			want: "/products",
		},
		{
			name: "modifies path for single product",
			args: args{path: []byte("/products/1"), id: "object-id"},
			want: "/products/1",
		},
		{
			name: "modifies path for prices",
			args: args{path: []byte("/products/1/prices"), id: "object-id"},
			want: "/products/1/suppliers/object-id/prices",
		},
		{
			name: "modifies path for single price",
			args: args{path: []byte("/products/1/prices/1"), id: "object-id"},
			want: "/products/1/suppliers/object-id/prices/1",
		},
		{
			name:    "non matching path returns error",
			args:    args{path: []byte("/any"), id: "object-id"},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := rewritePath(tt.args.path, tt.args.id)
			if (err != nil) != tt.wantErr {
				t.Errorf("rewritePath() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("rewritePath() got = %v, want %v", got, tt.want)
			}
		})
	}
}
