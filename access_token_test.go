package main

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"
	"time"
)

func TestAuth0Client_GetAccessToken(t *testing.T) {
	var endpointCallCounter uint32
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		endpointCallCounter++
		resp := auth0Response{
			AccessToken: "access-token",
			Scope:       "scope",
			ExpiresIn:   86400,
			TokenType:   "Bearer",
		}
		err := json.NewEncoder(w).Encode(resp)
		if err != nil {
			t.Fatalf("sendAuth0Request() error = %v", err)
		}
	}))
	defer ts.Close()

	type fields struct {
		auth0Token        *auth0Response
		auth0TokenCreated time.Time

		TokenEndpoint string
		ClientId      string
		ClientSecret  string
		Audience      string
		GrantType     string
	}
	tests := []struct {
		name   string
		fields fields
		want   string
		wantC  uint32
	}{
		{
			name:   "Auth0 called when does not have cache",
			fields: fields{TokenEndpoint: ts.URL},
			want:   "access-token",
			wantC:  1,
		},
		{
			name: "Auth0 not called when have cache",
			fields: fields{
				TokenEndpoint:     ts.URL,
				auth0Token:        &auth0Response{AccessToken: "access-token", ExpiresIn: int64(3600)},
				auth0TokenCreated: time.Now(),
			},
			want:  "access-token",
			wantC: 0,
		},
		{
			name: "Auth0 called when cache expired",
			fields: fields{
				TokenEndpoint:     ts.URL,
				auth0Token:        &auth0Response{AccessToken: "access-token", ExpiresIn: int64(3600)},
				auth0TokenCreated: time.Now().Add(-time.Hour),
			},
			want:  "access-token",
			wantC: 1,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			endpointCallCounter = 0
			c := &Auth0Client{
				auth0Token:        tt.fields.auth0Token,
				auth0TokenCreated: tt.fields.auth0TokenCreated,

				TokenEndpoint: tt.fields.TokenEndpoint,
				ClientId:      tt.fields.ClientId,
				ClientSecret:  tt.fields.ClientSecret,
				Audience:      tt.fields.Audience,
				GrantType:     tt.fields.GrantType,
			}
			if got := c.GetAccessToken(); got != tt.want {
				t.Errorf("GetAccessToken() = %v, want %v", got, tt.want)
			}
			if got := endpointCallCounter; got != tt.wantC {
				t.Errorf("GetAccessToken() numberOfCalls = %v, want %v", got, tt.wantC)
			}
		})
	}
}

func Test_sendAuth0Request(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		resp := auth0Response{
			AccessToken: "access-token",
			Scope:       "scope",
			ExpiresIn:   86400,
			TokenType:   "Bearer",
		}
		err := json.NewEncoder(w).Encode(resp)
		if err != nil {
			t.Fatalf("sendAuth0Request() error = %v", err)
		}
	}))
	defer ts.Close()

	args := struct {
		url     string
		request auth0Request
	}{
		url:     ts.URL,
		request: auth0Request{},
	}
	want := &auth0Response{
		AccessToken: "access-token",
		Scope:       "scope",
		ExpiresIn:   86400,
		TokenType:   "Bearer",
	}

	got, err := sendAuth0Request(args.url, args.request)
	if err != nil {
		t.Errorf("sendAuth0Request() error = %v", err)
		return
	}
	if !reflect.DeepEqual(got, want) {
		t.Errorf("sendAuth0Request() = %v, want %v", got, want)
	}
}
