package main

import (
	"errors"
	"fmt"
	"golang.org/x/crypto/bcrypt"
	"net/http"
	"regexp"
)

var (
	productRouteRegexp = regexp.MustCompile(`^(/products(?:/\d+)?)$`)
	priceRouteRegexp   = regexp.MustCompile(`^(/products/\d+)(/prices(?:/\d+)?)$`)
)

type Supply struct {
	client     AccessTokenClient
	repository Repository
	proxy      http.Handler
}

func (s *Supply) Handler(writer http.ResponseWriter, request *http.Request) {
	defer func() {
		if r := recover(); r != nil {
			http.Error(writer, "500 Internal Server Error", http.StatusInternalServerError)
		}
	}()

	username, password, ok := request.BasicAuth()
	if !ok {
		http.Error(writer, "403 Forbidden", http.StatusForbidden)
		return
	}

	user, err := s.repository.findUserByName(username)
	if err != nil {
		http.Error(writer, "403 Forbidden", http.StatusForbidden)
		return
	}

	if err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(password)); err != nil {
		http.Error(writer, "403 Forbidden", http.StatusForbidden)
		return
	}

	path := []byte(request.URL.Path)
	p, err := rewritePath(path, user.Id)
	if err != nil {
		http.NotFound(writer, request)
		return
	}

	request.URL.Path = p
	token := s.client.GetAccessToken()
	request.Header.Set("X-Forwarded-Host", request.Header.Get("Host"))
	request.Header.Set("Authorization", "Bearer "+token)

	s.proxy.ServeHTTP(writer, request)
}

func rewritePath(path []byte, id string) (string, error) {
	if productRouteRegexp.Match(path) {
		results := productRouteRegexp.FindSubmatch(path)
		return fmt.Sprintf("%s", results[1]), nil
	}
	if priceRouteRegexp.Match(path) {
		results := priceRouteRegexp.FindSubmatch(path)
		return fmt.Sprintf("%s/suppliers/%s%s", results[1], id, results[2]), nil
	}

	return "", errors.New("path not found")
}
