FROM golang:alpine

ENV CGO_ENABLED=0
ENV PROCA_SUPPLY_GATEWAY_SOURCE="/proca/users.csv"
ENV PROCA_SUPPLY_GATEWAY_TARGET="http://localhost:8120"

EXPOSE 8210

COPY . /workspace/app
WORKDIR /workspace/app
RUN go build -o ./build/supply-gateway

ENTRYPOINT /workspace/app/build/supply-gateway \
    -source=${PROCA_SUPPLY_GATEWAY_SOURCE} \
    -target=${PROCA_SUPPLY_GATEWAY_TARGET} \
    -client-id=${PROCA_SUPPLY_GATEWAY_CLIENT_ID} \
    -client-secret=${PROCA_SUPPLY_GATEWAY_CLIENT_SECRET} \
    -audience=${PROCA_SUPPLY_GATEWAY_AUIDENCE} \
    -grant-type=${PROCA_SUPPLY_GATEWAY_GRANT_TYPE} \
/
