package main

import (
	"bytes"
	"context"
	"encoding/json"
	"net/http"
	"sync"
	"time"
)

type AccessTokenClient interface {
	GetAccessToken() string
}

type auth0Request struct {
	ClientId     string `json:"client_id"`
	ClientSecret string `json:"client_secret"`
	Audience     string `json:"audience,omitempty"`
	GrantType    string `json:"grant_type"`
}

type auth0Response struct {
	AccessToken string `json:"access_token"`
	Scope       string `json:"scope"`
	ExpiresIn   int64  `json:"expires_in"`
	TokenType   string `json:"token_type"`
}

type Auth0Client struct {
	auth0Token        *auth0Response
	auth0TokenCreated time.Time
	auth0TokenMutex   sync.Mutex

	TokenEndpoint string
	ClientId      string
	ClientSecret  string
	Audience      string
	GrantType     string
}

func (c *Auth0Client) validUntil() time.Time {
	return c.auth0TokenCreated.Add(time.Duration(c.auth0Token.ExpiresIn) * time.Second / 2)
}

func (c *Auth0Client) GetAccessToken() string {
	c.auth0TokenMutex.Lock()
	defer c.auth0TokenMutex.Unlock()

	if c.auth0Token == nil || c.validUntil().Before(time.Now()) {
		r := auth0Request{
			ClientId:     c.ClientId,
			ClientSecret: c.ClientSecret,
			Audience:     c.Audience,
			GrantType:    c.GrantType,
		}
		c.auth0Token, _ = sendAuth0Request(c.TokenEndpoint, r)
		c.auth0TokenCreated = time.Now()
	}

	return c.auth0Token.AccessToken
}

func sendAuth0Request(url string, request auth0Request) (*auth0Response, error) {
	b, _ := json.Marshal(request)
	p := bytes.NewReader(b)

	ctx, _ := context.WithTimeout(context.Background(), 3*time.Second)
	req, err := http.NewRequestWithContext(ctx, "POST", url, p)
	if err != nil {
		return nil, err
	}
	req.Header.Add("content-type", "application/json")
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}

	defer func() {
		_ = res.Body.Close()
	}()
	var response auth0Response
	err = json.NewDecoder(res.Body).Decode(&response)
	if err != nil {
		return nil, err
	}
	return &response, nil
}
