package main

import (
	"io"
	"reflect"
	"strings"
	"testing"
)

func TestInMemoryRepositoryFromCsv(t *testing.T) {
	type args struct {
		r io.Reader
	}
	tests := []struct {
		name    string
		args    args
		want    *InMemoryRepository
		wantErr bool
	}{
		{
			name:    "Reader is nil",
			args:    args{r: nil},
			wantErr: true,
		},
		{
			name:    "Reader contain less column",
			args:    args{r: strings.NewReader(",")},
			wantErr: true,
		},
		{
			name:    "Reader contain more column",
			args:    args{r: strings.NewReader(",,,")},
			wantErr: true,
		},
		{
			name:    "Reader contain mixed column",
			args:    args{r: strings.NewReader(",,\n,")},
			wantErr: true,
		},
		{
			name: "Reader contain more column",
			args: args{r: strings.NewReader("id,username,password")},
			want: &InMemoryRepository{users: []User{{Id: "id", Username: "username", Password: "password"}}},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := InMemoryRepositoryFromCsv(tt.args.r)
			if (err != nil) != tt.wantErr {
				t.Errorf("InMemoryRepositoryFromCsv() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("InMemoryRepositoryFromCsv() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestInMemoryRepository_findUserByName(t *testing.T) {
	type fields struct {
		users []User
	}
	type args struct {
		username string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantS   User
		wantErr bool
	}{
		{
			name:    "User does not exist",
			fields:  fields{users: []User{{Username: "user1"}}},
			args:    args{username: "none"},
			wantErr: true,
		},
		{
			name:   "User exists",
			fields: fields{users: []User{{Username: "user1"}}},
			args:   args{username: "user1"},
			wantS:  User{Username: "user1"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			i := &InMemoryRepository{
				users: tt.fields.users,
			}
			gotS, err := i.findUserByName(tt.args.username)
			if (err != nil) != tt.wantErr {
				t.Errorf("findUserByName() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotS, tt.wantS) {
				t.Errorf("findUserByName() gotS = %v, want %v", gotS, tt.wantS)
			}
		})
	}
}
